#!/usr/bin/env python

import os, shutil, argparse
import sys

# Parse command options.
parser = argparse.ArgumentParser(
    description = "Script to generate GenXicc grid files.")
parser.add_argument("-c", "--config", type = str,
                    default = "$GAUSSOPTS/Gauss-2012.py",
                    help = "the beam configuration file")
parser.add_argument("-o", "--output", type = str,
                    default = "8TeV",
                    help = "the output directory")
parser.add_argument("-s", "--state", type = str, default = "Xi_cc++",
                    help = "state for which grid file should be generated.  Possible options are Omega_cc+, Xi_bc+, Xi_bc0, Xi_cc+, Xi_cc++")
parser.add_argument("-t", "--test", action = "store_true",
                    help = "flag to run a fast test run")
args = parser.parse_args()



eventTypes = { 'Omega_cc+': '26165856',
               'Xi_bc+' : '16675061',
               'Xi_bc0' : '16876060',
               'Xi_cc+' : '26163051',
               'Xi_cc++' : '26166061'
             }

directories = {
    'Omega_cc+' : 'xiccs',
    'Xi_bc+' : 'xibcu',
    'Xi_bc0' : 'xibcd',
    'Xi_cc+' : 'xiccd',
    'Xi_cc++' : 'xiccu'
}

if args.state not in directories:
    print('State {} not yet supported. Please modify me to add corresponding event type and directory'.format(args.state))
    sys.exit()

# Check whether we have main directory
if not os.path.isdir("../data/" + args.output + '/data'):
    print('Directory ../data/' + args.output + '/data does not exist.  Please create one and provide symlinks to 7 TeV directory for relevant PDF files')
    sys.exit()

# Check whether we have specific grid file and if not, create empty one
# This list might want to get more complicated if we ever want to set
# ixiccstate to anything else than 1 (default in Gauss)
gradeFiles = {
    'Omega_cc+' : 'gg3s1c3.grid',
    'Xi_bc+' : 'admixgg.grid',
    'Xi_bc0' : 'admixgg.grid',
    'Xi_cc+' : 'gg3s1c3.grid',
    'Xi_cc++' : 'gg3s1c3.grid'
}

if not os.path.exists('../data/' + args.output + '/data/' + directories[args.state] + '/' + gradeFiles[args.state]):
    os.makedirs('../data/' + args.output + '/data/' + directories[args.state], exist_ok=True)
    with open('../data/' + args.output + '/data/' + directories[args.state] + '/' + gradeFiles[args.state], mode='a'): pass

# Create the configuration file.
opts = [
    "# Import the necessary options.",
    "from Gaudi.Configuration import importOptions",
    "importOptions('$GAUSSOPTS/GenStandAlone.py')",
    "importOptions('$DECFILESROOT/options/{0}.py')".format(eventTypes[args.state]),
#    "importOptions('$LBPYTHIA8ROOT/options/Pythia8.py')",
    "importOptions('$LBGENXICCROOT/options/GenXiccPythia8.py')",
    "importOptions(%r)" % args.config,
    "",
    "# Configure Gauss.",
    "from Gauss.Configuration import GenInit, LHCbApp",
    "GaussGen = GenInit('GaussGen')",
    "GaussGen.FirstEventNumber = 1",
    "GaussGen.RunNumber = 1",
    "LHCbApp().EvtMax = 1",
    "",
    "# Add GenXicc as special production tool and instruct to build grids.",
    "from Configurables import Generation, Special, GenXiccProduction",
    "Generation().addTool(Special)",
    "Generation().Special.ProductionTool = 'GenXiccProduction'",
    "Generation().Special.addTool(GenXiccProduction)",
    "Generation().Special.GenXiccProduction.Commands += [",
    "    'loggrade ivegasopen 1',"
]
if args.test: opts += [
        "    'vegasinf number 1000', # Number of sampling points.",
        "    'vegasinf nitmx 20',  # Maximum number of iterations.",
]
else:
    opts += [
        "    'vegasinf number 1000000', # Number of sampling points.",
        "    'vegasinf nitmx 20',  # Maximum number of iterations.",
    ]
opts += ["]"]
cfg = open("cfg.py", "w")
for opt in opts: cfg.write(opt + "\n")
cfg.close()
    
# Run the command to create the grids in place.
cmd = "gaudirun.py cfg.py"
os.system(cmd)
