# GenXiccData package

This package contains grid files for GenXicc event generators needed for productions. Separate grid files for each beam energy are required.

## Generating new grid files

The grid files are generated directly by the GenXicc event generator during initialisation phase. It requires special options to be set. To facilitate this, script `scripts/grids.py` is provided. Usage to generate grid files is:
```bash
# Get local copy of GenXiccData package
git clone ssh://git@gitlab.cern.ch:7999/lhcb-datapkg/GenXiccData.git
cd GenXiccData
git checkout -b <your new branch>
# Setup Gauss runtime environment (released Sim10 version in this case)
lb-run Gauss/v56r3 bash
# Point Gauss to your local copy of GenXiccData
export GENXICCDATAROOT=<path to your local copy>
# Go to GenXiccData/scripts directory
cd doc
# Run grid generation
# Recommend first trying with -t option for quick test that everything is setup properly
# Following states are supported:  Omega_cc+, Xi_bc+, Xi_bc0, Xi_cc+, Xi_cc++
# Example here generates grids for 8 TeV, change Gauss-2012.py to appropriate year
./grids.py --config $GAUSSOPTS/Gauss-2012.py --output 8TeV -s Xi_cc++
```
Afterwards commit, push branch to gitlab and make merge request.

In case you are generating first grid file for given energy, first before running `grids.py` create directory `data/<energy>TeV/data` and in it link files `cteq6hq.dat` and  `cteq6hq.pds` from `data/7TeV/data`. Additionally, in Gauss project,  `Gen/GenXicc/src/outerpdf.F` and `Gen/GenXicc/src/upopenfile.F` need to be updated to recognize new energy and translate it to correct subdirectory in GenXiccData package.

Final note, script currently generates only default production mechanisms used in Gauss, if additional production mechanism (selected by ixiccstate variable) is needed, script will have to be adapted to translate to correct names. If new state is needed, again script needs update to pick up suitable event type and correct subdirectory for grid files. 

